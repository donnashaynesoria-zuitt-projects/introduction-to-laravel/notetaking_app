<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Post
Route::get('/notes/create', 'PostController@create');
Route::post('/notes', 'PostController@store');
Route::get('/notes', 'PostController@index');
Route::get('/notes/my-notes','PostController@myPosts');
Route::get('/notes/my-notes/allArchived','PostController@allArchived');
Route::get('/notes/{post_id}', 'PostController@show');
Route::put('/notes/{post_id}','PostController@update');
Route::get('/notes/{post_id}/edit','PostController@edit');
Route::delete('/notes/{post_id}','PostController@destroy');
Route::put('/notes/{post_id}/archive', 'PostController@archive');
